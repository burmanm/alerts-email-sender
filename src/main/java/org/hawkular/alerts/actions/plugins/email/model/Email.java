package org.hawkular.alerts.actions.plugins.email.model;

import java.util.HashSet;
import java.util.Set;

public class Email {
    public String subject;
    public String body;
    public Set<String> recipients;
    public String bodyType;

    public Email() {
        this.recipients = new HashSet<>();
    }

    public void addRecipient(String recipientName, String recipient) {
        // Parse here for proper format.. or something
        String to = String.format("\"%s\" %s", recipientName, recipient);
        this.recipients.add(to);
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBody(String body, String bodyType) {
        this.body = body;
        this.bodyType = bodyType;
    }
}
