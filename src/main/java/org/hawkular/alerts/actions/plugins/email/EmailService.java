package org.hawkular.alerts.actions.plugins.email;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.hawkular.alerts.actions.plugins.email.model.Emails;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface EmailService {

    @POST
    @Path("/emails")
    String sendMail(@HeaderParam("x-rh-apitoken") String apiToken, Emails emails);
}
