package org.hawkular.alerts.actions.plugins.email;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.hawkular.alerts.actions.api.ActionMessage;
import org.hawkular.alerts.actions.plugins.email.model.Email;
import org.hawkular.alerts.actions.plugins.email.model.Emails;
import org.hawkular.alerts.api.json.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.hawkular.alerts.api.util.Util.isEmpty;

@ApplicationScoped
public class EmailPlugin {
    private final Logger log = LoggerFactory.getLogger(EmailPlugin.class);

    public static final String HAWKULAR_ALERTS_TEMPLATES = "HAWKULAR_ALERTS_TEMPLATES";
    public static final String HAWKULAR_ALERTS_TEMPLATES_PROPERY = "hawkular.alerts.templates";

    /*
        This is the list of properties supported for the Email plugin.
        Properties are personalized per action.
        If not properties found per action, then plugin looks into default properties set at plugin level.
        If not default properties found at plugin level, then it takes to default ones defined inside plugin.
     */

    @Inject
    @RestClient
    EmailService emailService;

    @ConfigProperty(name = "com.redhat.cloud.custompolicies.engine.actions.plugins.email.from")
    String from;

    @ConfigProperty(name = "com.redhat.cloud.custompolicies.engine.actions.plugins.email.apitoken")
    String apiToken;

    /**
     * "to" property defines the recipient of the plugin email.
     */
    public static final String PROP_TO = "to";

    /**
     * "template.hawkular.url" property defines the URL that will be used in the template email to point to hawkular
     * server. If not "template.hawkular.url" defined, then the plugin looks into system env HAWKULAR_BASE_URL.
     */
    @ConfigProperty(name = "com.redhat.cloud.custompolicies.engine.actions.plugins.email.template.hawkular.url")
    String serverUrl;

    /**
     * Email plugin supports localization templates.
     * "template.locale" is the property used to define which template to use for specific locale.
     * i.e. A plugin may have defined several templates to support multiple locales [es,en,fr], but we can define a
     * specific locale per action [es].
     */
    public static final String PROP_TEMPLATE_LOCALE = "template.locale";

    /**
     * "template.plain" property defines the template used for plain text email.
     * Additional "template.plain" properties can be defined to support localization:
     * - "template.plain.LOCALE": where LOCALE is a variable that can point to specific localization.
     *
     * Templates are plain text based on http://freemarker.org/ engine.
     * Email plugin processes the alert payload and adds a set of pre-defined variables to be used into the template.
     * The list of variables available for templates are wrapped into {@see PluginMessageDescription} class.
     *
     */
    public static final String PROP_TEMPLATE_PLAIN = "template.plain";

    /**
     * "template.html" property defines the template used for html email.
     * Additional "template.html" properties can be defined to support localization:
     * - "template.html.LOCALE": where LOCALE is a variable that can point to specific localization.
     *
     * Email plugin uses templates based on http://freemarker.org/ engine.
     * Email plugin processes the alert payload and adds a set of pre-defined variables to be used into the template.
     * The list of variables available for templates are wrapped into {@see PluginMessageDescription} class.
     *
     */
    public static final String PROP_TEMPLATE_HTML = "template.html";

    private EmailTemplate emailTemplate;

    public EmailPlugin() {
        emailTemplate = new EmailTemplate();
    }

    @Incoming("email")
    public void process(String json) {
        ActionMessage msg = JsonUtil.fromJson(json, ActionMessage.class);
        try {
            Emails emails = new Emails();
            Email email = createEmail(msg);
            Set<Email> mails = new HashSet<>();
            mails.add(email);
            emails.emails = mails;

            // Send it..
            emailService.sendMail(apiToken, emails);
        } catch (Exception e) {
            // What to do with errors? Where do we report them, what should we do with the messages?
            e.printStackTrace();
        }

    }

    protected Email createEmail(ActionMessage msg) throws Exception {
        Email email = new Email();

        // TODO How do we support multiple recipients? Comma separated list?
        String to = msg.getAction().getProperties().get(PROP_TO);
        if (!isEmpty(to)) {
            // TODO Do we really need recipient name?
            email.addRecipient("Report receiver", to);
        }

        Map<String, String> emailProcessed = emailTemplate.processTemplate(msg);

        String subject = emailProcessed.get("emailSubject");
        if (!isEmpty(subject)) {
            email.setSubject(subject);
        } else {
            log.debug("Subject not found processing email on message: {}", msg);
        }

        String plain = emailProcessed.get("emailBodyPlain");
        String html = emailProcessed.get("emailBodyHtml");
        if(html != null) {
            // TODO How do we select which one to send?
            email.setBody(html, "html");
        }
        return email;
    }
}
