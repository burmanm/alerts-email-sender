package org.hawkular.alerts.plugins.email;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.hawkular.alerts.actions.plugins.email.EmailService;
import org.hawkular.alerts.actions.plugins.email.model.Email;
import org.hawkular.alerts.actions.plugins.email.model.Emails;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.Set;

//@QuarkusTest
public class EmailSendTest {

    @Inject
    @RestClient
    EmailService emailService;

//    @Test
    public void testEndpoint() {
        Emails emails = new Emails();

        Email email = new Email();
        email.addRecipient("Hello Receiver", "hello@hello.com");
        email.setBody("<html>mail!</html>", "html");
        email.setSubject("hello my friend");
        Set<Email> objects = new HashSet<>();
        objects.add(email);

        emails.emails = objects;
        emailService.sendMail("hello", emails);
    }
}
